# README #

This script automates a matrix of features given OHLC market price data and user inputs for
currency type, time horizon, and number of averaging periods. Features comprise GARCH model forecasts, 
signal processing, and technical analysis, all of which have been derived internally and validated
against external libraries. The matrix is used in subsequent scripts for feature selection and feature extraction.
